/**
 * Dashboard component entry.
 * @author qinmu
 */

'use strict'

import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import accountMutations from 'modules/Account/mutations'
import Component from './component'
import mutations from './mutations'
import { submitNewTranslationAction } from './actions'

const mapStateToProps = ({ dashboard, account, router }) => {
  const {
    requestingItems,
    submitResult,
    pageList,
    websiteList,
    totalPages,
    totalWebsites,
  } = dashboard

  const { accountInfo } = account

  // Deascending
  const sortedWebsiteList = Object.values(websiteList).sort((a, b) => {
    if (a.updated_at < b.updated_at) {
      return 1
    } else {
      return -1
    }
  })

  return {
    accountInfo,
    router,
    requestingItems,
    submitResult,
    pageList,
    websiteList,
    sortedWebsiteList,
    totalPages,
    totalWebsites,
  }
}

const mapDispatchToProps = dispatch => ({
  submitNewTranslation: ({ metaData, category }) => {
    dispatch(submitNewTranslationAction({ metaData, category }))
  },

  goLoadPages: ({ requestParams }) => (dispatch({
    type: mutations['GET_PAGE_LIST_SAGA'],
    requestParams,
  })),

  goLoadWebsites: ({ requestParams }) => (dispatch({
    type: mutations['GET_WEBSIE_LIST_SAGA'],
    requestParams,
  })),

  goLoadWebsiteMorePages: ({ requestParams }) => (dispatch({
    type: mutations['GET_WEBSITE_MORE_PAGES_SAGA'],
    requestParams,
  })),

  goLogout: () => (dispatch({
    type: accountMutations['LOG_OUT_SAGA'],
  })),

  goDeletePage: (pageId) => (dispatch({
    type: mutations['DELETE_PAGE_SAGA'],
    pageId,
  })),

  goDeleteWebsite: (websiteId) => (dispatch({
    type: mutations['DELETE_WEBSITE_SAGA'],
    websiteId
  }))
})

const Dashboard = connect(mapStateToProps, mapDispatchToProps)(Component)

export default withRouter(Dashboard)
