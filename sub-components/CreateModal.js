/**
 * Modal for creating an new translation
 */
'use strict'

import React from 'react'
import { PropTypes } from 'prop-types'
import { Form, Modal, Input, Radio } from 'antd'
import {
  displayErrorMsg,
} from 'utils/messages'

const FormItem = Form.Item

class CreateTranslationModal extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      pageType: 'article',
      targetLang: 'en',
      url: '',
    }
  }

  componentWillReceiveProps({ submitResult }) {
    if (submitResult !== this.props.submitResult
        && submitResult === 'success') {
      this.handleCancel()
    }
  }

  setFormField = ({ target }) => this.setState({ url: target.value })
  handleTypeChange = (e) => this.setState({ pageType: e.target.value })
  handleTargetLangChange = (e) => this.setState({ targetLang: e.target.value })

  clearFieldValues = () => {
    this.setState({
      pageType: 'article',
      targetLang: 'en',
      url: '',
    })
  }

  handleCancel = () => {
    this.props.closeModal()
    this.clearFieldValues()
  }

  handleSubmit = () => {
    // 提交的时候，注明来自官网。及某个用户。
    const { url, pageType, targetLang } = this.state
    const { submitNewTranslation } = this.props

    let errMsg = ''
    if (!url.startsWith('http')) {
      errMsg = '请输入有效的网址'
    } else if (!pageType) {
      errMsg = '请填写翻译类型'
    } else if (!targetLang) {
      errMsg = '请填写要翻译的目标语言'
    }

    if (errMsg) {
      displayErrorMsg(errMsg)
      return
    }

    const allLangsMap = {
      'en': 'zh-CN',
      'zh-CN': 'en',
    }
    const sourceLang = allLangsMap[targetLang]
    
    let category = ''
    let metaData = {
      'source_lang': sourceLang,
      'target_lang': targetLang,
    }

    if (pageType === 'website') {
      category = 'website'
      metaData = { ...metaData, ...{ url } }
    } else {
      category = 'page'
      metaData = { ...metaData, ...{ 'page_url': url, 'page_type': pageType } }
    }

    submitNewTranslation({ metaData, category })
  }

  render() {
    const {
      isShowModal,
      requestingItems,
    } = this.props

    const { pageType, targetLang, url } = this.state

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    }

    return (
      <Modal
        title="提交新的翻译"
        visible={isShowModal}
        okText="提交"
        cancelText="取消"
        confirmLoading={requestingItems.includes('submitTranslation')}
        afterClose={this.clearFieldValues}
        onOk={this.handleSubmit}
        onCancel={this.handleCancel}
      >
        <Form>
          <FormItem
            {...formItemLayout}
            label="网址"
            required
          >
            <Input
              type="text"
              value={url}
              onChange={this.setFormField}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="翻译类型"
            required
          >
            <Radio.Group
              value={pageType}
              onChange={this.handleTypeChange}
            >
              <Radio.Button value="article">Article</Radio.Button>
              <Radio.Button value="website">Website</Radio.Button>
            </Radio.Group>
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="目标语言"
            required
          >
            <Radio.Group
              value={targetLang}
              onChange={this.handleTargetLangChange}
            >
              <Radio value='en'>英语</Radio>
              <Radio value='zh-CN'>简体中文</Radio>
            </Radio.Group>
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

CreateTranslationModal.propTypes = {
  isShowModal: PropTypes.bool.isRequired,
  requestingItems: PropTypes.array.isRequired,
  submitResult: PropTypes.string.isRequired,
  closeModal: PropTypes.func.isRequired,
  submitNewTranslation: PropTypes.func.isRequired,
}

export default CreateTranslationModal
