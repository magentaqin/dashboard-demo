/**
 * Dashboard Header
 */

'use strict'

import React from 'react'
import styled from 'styled-components'
import { Layout, Icon, Badge, Button } from 'antd'
import { PropTypes } from 'prop-types'

const Wrapper = styled.div`
  width: 80%;
  max-width: 1600px;
  margin: 0 auto;
`
const Logo = styled.div`
  display: inline-block;
  font-size: 24px;
  color: #fff;
`
const Menu = styled.div`
  float: right;
  color: #fff;
  font-size: 16px;
  display: flex;
  align-items: center;
`
const MenuIcon = styled(Icon) `
  font-size: 24px;
  line-height: 64px;
  padding-left: 10px;
  padding-right: 10px;

  &:hover {
    background-color: white;
    color: #001529;
    cursor: pointer;
  }
`
const MenuNote = MenuIcon.extend`
  padding: 0;
  line-height: inherit;
  color: #fff !important;
  margin-left: 10px;
  padding-bottom: 9px;

  &:hover {
    background-color: inherit;
  }
`
const Username = styled.span`
  margin-left: 20px;
`
const Header = ({
  username,
  createNewTranslation,
  showNotification,
  notificationsCount,
  goLogout,
}) => (
    <Layout.Header>
      <Wrapper>
        <Logo>Breword Translators</Logo>
        <Menu>
          <MenuIcon type="plus" onClick={createNewTranslation} />
          <Badge count={notificationsCount} dot>
            <MenuNote type="notification" onClick={showNotification} />
          </Badge>
          <Username>欢迎回来，{username}</Username>
          <Button
            onClick={goLogout}
            style={{ marginLeft: 20 }}
            type="default"
            ghost
          >
            退出<Icon type="logout" />
          </Button>
        </Menu>
      </Wrapper>
    </Layout.Header>
  )

Header.propTypes = {
  username: PropTypes.string.isRequired,
  createNewTranslation: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired,
  notificationsCount: PropTypes.number.isRequired,
  goLogout: PropTypes.func.isRequired,
}

export default Header