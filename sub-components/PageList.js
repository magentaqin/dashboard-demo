/**
 * Render page list
 */
'use strict'

import React from 'react'
import { Link } from "react-router-dom"
import { PropTypes } from 'prop-types'
import styled from 'styled-components'
import { Icon, List, Button, Tooltip } from 'antd'
import { DataLoading } from 'components/Loading'

import { formatLocalDatetime } from 'utils/datetime'
import { PAGE_TYPES, LANGUAGES_MAP } from 'utils/constants'
import copyTextToClipboard from 'utils/copyTextToClipboard'
import { getPageSourceUrlApi } from 'services/translateService'
import { displaySuccessMsg } from 'utils/messages'
import { appendQueryStringToUrl } from 'utils/qs'

import DeleteModal from './DeleteModal'

const ListItem = List.Item

const ListItemWrap = styled(ListItem) `
  .ant-list-item-meta-description {
    height: 32px;
    display: flex;
  }

  &:hover {
    cursor: pointer;

   .actions-group, button {
      display:flex;
    }

    .description-url {
      display: none;
    }
  }
`
const LoadMoreWrap = styled.div`
  height: 32px;
  line-height: 32px;
  text-align: center;
  margin-top: 12px;
  margin-bottom: 12px;
`
const ActionsGroup = styled.div`
  float: right;
  display: none;
  width: 120px;
  justify-content: space-between;

  button, a {
    font-size: 18px;
    border: none;
  }

  a {
    display: inline-block;
    line-height: 32px;
    color: rgba(0,0,0,.65);

    &:hover {
      color: #40a9ff;
    }
  }
`
const TitleWrap = styled.div`
  display: flex;
  justify-content: space-between;

  button {
    font-size: 18px;
    border: none;
    display: none;
  }
`
const ItemMetadata = styled.div`
  line-height: 32px;
`

const handleProgress = (number) => {
  return parseFloat(number) > 0 ? parseInt(number * 100) + '%' : '暂未翻译'
}

const copyTargetPageUrl = id => () => {
  const url = getPageSourceUrlApi({ pageId: id })
  copyTextToClipboard(url)
  displaySuccessMsg('复制译文页面地址成功!')
}

const PageList = ({
  requestingItems,
  pageList,
  totalPages,
  goLoadMore,
  goDeletePage,
  deletingPageId,
  setDeletingPageId,
  activeId,
  setActiveId
}) => {
  const isLoading = requestingItems.includes('pages')
  const isDeleting = requestingItems.includes('deletePage')
  const isNoMoreData = pageList.length === totalPages

  const loadMore = (
    <LoadMoreWrap>
      {isLoading && <DataLoading />}
      {!isLoading && isNoMoreData && <span>没有更多了</span>}
      {!isLoading && !isNoMoreData && <Button onClick={goLoadMore}>加载更多</Button>}
    </LoadMoreWrap>
  )

  const IconText = ({ title, text }) => ( // eslint-disable-line react/prop-types
    <span>
      <span style={{ marginRight: 8 }}>{title}:</span>
      {text}
    </span>
  )

  const handleDeletePage = id => {
    if (isDeleting) {
      return
    }
    setDeletingPageId(id)
    goDeletePage(id)
  }

  const generateEditorUrl = ({ id, url }) => appendQueryStringToUrl('/editor', {
    page_type: 'page',
    page_id: id,
    url,
  })

  const initActiveId = () => {
    setActiveId('')
  }

  const ItemTitle = ({ id, url, title, source_lang, target_lang }) => ( // eslint-disable-line react/prop-types
    <TitleWrap>
      <ItemMetadata>
        <Link to={generateEditorUrl({ id, url })}>{title}</Link>
        <span style={{ marginLeft: '60px' }}>
          <span>{source_lang}</span>
          <Icon
            type="arrow-right"
            style={{ marginLeft: '8px', marginRight: '8px' }}
          />
          <span>{target_lang}</span>
        </span>
      </ItemMetadata>
      <Tooltip title="删除">
        <Button
          icon="delete"
          style={{ display: activeId === id ? 'flex': ''}}
          loading={isDeleting && deletingPageId === id}
          onClick={() => {
            setActiveId(id)
            DeleteModal(handleDeletePage, id, title, initActiveId)
          }}
        />
      </Tooltip>
    </TitleWrap>
  )

  const ItemAction = ({ id, url }) => ( // eslint-disable-line react/prop-types
    <ActionsGroup
      className="actions-group"
      style={{ display: activeId === id ? 'flex' : '' }}
    >
      <Tooltip title="复制译文地址">
        <Button icon="copy" onClick={copyTargetPageUrl(id)} />
      </Tooltip>
      <Tooltip title="打开原网页">
        <a href={url} target="_blank">
          <Icon type="link" />
        </a>
      </Tooltip>
      <Tooltip title="预览">
        <a href={getPageSourceUrlApi({ pageId: id })} target="_blank">
          <Icon type="play-circle-o" />
        </a>
      </Tooltip>
    </ActionsGroup>
  )

  const ItemDescription = ({ id, url}) => ( // eslint-disable-line react/prop-types
    <div>
      <p className="description-url" style={{ display: activeId === id ? 'none' : ''}}>{url}</p>
      <ItemAction id={id} url={url} />
    </div>
  )

  return (
    <List
      itemLayout="vertical"
      loadMore={loadMore}
      size="large"
      dataSource={pageList}
      footer={<div><b>页面总数：</b>{totalPages}</div>}
      renderItem={item => (
        <ListItemWrap
          key={item.id}
          actions={[
            <IconText key="1" title="字符总数" text={item.page_word_count} />,
            <IconText key="2" title="类型" text={PAGE_TYPES[item.page_type]} />,
            <IconText
              key="3"
              title="创建日期"
              text={formatLocalDatetime(item.created_at)}
            />,
            <IconText
              key="4"
              title="更新日期"
              text={formatLocalDatetime(item.updated_at)}
            />,
            <IconText key="5" title="译者" text={item.translator || '无'} />,
            <IconText
              key="6" title="完成度"
              text={handleProgress(item.translateProgress)}
            />,
          ]}
        >
          <ListItem.Meta
            title={
              <ItemTitle
                id={item.id}
                url={item.url}
                title={item.title}
                source_lang={LANGUAGES_MAP[item.source_lang]}
                target_lang={LANGUAGES_MAP[item.target_lang]}
              />
            }
            description={<ItemDescription id={item.id} url={item.url} />}
          />
        </ListItemWrap>
      )}
    />
  )
}

PageList.propTypes = {
  requestingItems: PropTypes.array.isRequired,
  pageList: PropTypes.array.isRequired,
  totalPages: PropTypes.number.isRequired,
  goLoadMore: PropTypes.func.isRequired,
  goDeletePage: PropTypes.func.isRequired,
  deletingPageId: PropTypes.string.isRequired,
  setDeletingPageId: PropTypes.func.isRequired,
  activeId: PropTypes.string.isRequired,
  setActiveId: PropTypes.func.isRequired
}

export default PageList
