/**
 * Notification component for display notificaitons
 * TODO: preserve to the future use.
 */
'use strict'

import React from 'react'
import { PropTypes } from 'prop-types'
import { Popover } from 'antd'
import styled from 'styled-components'

const MarkAllText = styled.a`
  float: right;
`

const Title = ({ markAllRead }) => (
  <div>
    <span>通知</span>
    <MarkAllText onClick={markAllRead}>全部标记为已读</MarkAllText>
  </div>
)

Title.propTypes = {
  markAllRead: PropTypes.func.isRequired,
}

const Notification = ({
  isShowNotificationPane,
  markAllRead,
}) => (
  <Popover
    content={<p>暂无更多消息</p>}
    title={<Title markAllRead={markAllRead} />}
    visible={isShowNotificationPane}
  />
)

Notification.propTypes = {
  isShowNotificationPane: PropTypes.bool.isRequired,
  markAllRead: PropTypes.func.isRequired,
}

export default Notification