/**
 * Dashboard Footer
 */ 

'use strict'

import React from 'react'
import styled from 'styled-components'
import { Layout } from 'antd'

const Wrapper = styled.div`
  width: 80%;
  max-width: 1600px;
  margin: 0 auto;
`
const StyledFooter = styled(Layout.Footer)`
  color: #333;
  background-color: #fff !important;
`
const Footer = () => (
  <StyledFooter>
    <Wrapper>copyright@breword.com - 2018</Wrapper>
  </StyledFooter>
)

export default Footer