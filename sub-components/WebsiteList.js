/**
 * Dashboard website list
 */
'use strict'

import React from 'react'
import { Link } from "react-router-dom"
import { PropTypes } from 'prop-types'
import styled from 'styled-components'
import { Button, Card, Collapse, Icon, List } from 'antd'
import { DataLoading } from 'components/Loading'
import { formatLocalDatetime } from 'utils/datetime'
import { appendQueryStringToUrl } from 'utils/qs'
import { LANGUAGES_MAP } from 'utils/constants'
import DeleteModal from './DeleteModal'


const StyledPanel = styled(Collapse.Panel) `
  border-radius: 2px;
  margin-bottom: 24px;
  border: 1px solid #ebedf0;
  overflow: hidden;
  position: relative;

  .ant-card {
    position: initial;
  }

  .delete-button {
    position: absolute;
    top: 12px;
    right: 4px;
    border: none;
    display: none;
  }

  &:hover {
    .delete-button {
      display: block;
    }
  }
`
const ContentLabel = styled.span`
  margin-right: 10px;
`
const ContentValue = styled.span`
`
const Content = styled.span`
  margin-right: 30px;

  &:last-child {
    margin-right: 0;
  }
`
const ContentRight = styled.span`
  display: inline-block;
  float: right;
`
const LoadMoreWrap = styled.div`
  height: 32px;
  line-height: 32px;
  text-align: center;
  margin-top: 12px;
  margin-bottom: 12px;
`

const handleProgress = (number) => {
  return parseFloat(number) > 0 ? parseInt(number * 100) + '%' : '暂未翻译'
}

const handleDelete = (e, goDeleteWebsite, id, title, setActiveId) => {
  e.stopPropagation()
  setActiveId(id)
  const initActiveId = () => { setActiveId('') }
  DeleteModal(goDeleteWebsite, id, title, initActiveId)
}

/* eslint-disable react/prop-types */
const HeaderTitle = ({ title, source_lang, target_lang }) => (
  <div>
    <span>{title}</span>
    <span style={{ marginLeft: '60px' }}>
      <span>{source_lang}</span>
      <Icon
        type="arrow-right"
        style={{ marginLeft: '8px', marginRight: '8px' }}
      />
      <span>{target_lang}</span>
    </span>
  </div>
)

const HeaderCard = ({
  title, site_url, word_count_total, total_pages,
  created_at, updated_at, source_lang,
  target_lang, translateProgress, id
}, goDeleteWebsite, activeId, setActiveId, isDeleting) => (
    <Card title={<HeaderTitle
      title={title}
      source_lang={LANGUAGES_MAP[source_lang]}
      target_lang={LANGUAGES_MAP[target_lang]}
    />}
      extra={<span>翻译进度：<b>{handleProgress(translateProgress)}</b></span>}
    >
      <Button
        style={{ display: activeId === id ? 'flex': ''}}
        loading={ isDeleting && activeId === id }
        icon="delete"
        className="delete-button"
        onClick={(e) => handleDelete(e, goDeleteWebsite, id, title, setActiveId)}
      />
      <p>
        <Content>
          <ContentLabel>网站地址:</ContentLabel>
          <ContentValue><a href={site_url} target="_blank">{site_url}</a></ContentValue>
        </Content>
        <ContentRight>
          <Content>
            <ContentLabel>创建日期:</ContentLabel>
            <ContentValue>{formatLocalDatetime(created_at)}</ContentValue>
          </Content>
          <Content>
            <ContentLabel>更新日期:</ContentLabel>
            <ContentValue>{formatLocalDatetime(updated_at)}</ContentValue>
          </Content>
          {/* <Content>   TODO: Add the feature next version.
          <ContentLabel>主要译者:</ContentLabel>
          <ContentValue>{translator || '无'}</ContentValue>
        </Content> */}
        </ContentRight>
      </p>
      <div>
        <Content>
          <ContentLabel>总字数:</ContentLabel>
          <ContentValue>{word_count_total}</ContentValue>
        </Content>
        <Content>
          <ContentLabel>页面数量:</ContentLabel>
          <ContentValue>{total_pages}</ContentValue>
        </Content>
      </div>
    </Card>
  )

const LoadMoreWebsites = ({ isNoMoreData, loadingMore, goLoadMore }) => (
  <LoadMoreWrap>
    {loadingMore && <DataLoading />}
    {!loadingMore && isNoMoreData && <span>没有更多了</span>}
    {!loadingMore && !isNoMoreData && <Button onClick={goLoadMore}>加载更多网站</Button>}
  </LoadMoreWrap>
)

const PageList = ({
  websiteId,
  pageList,
  totalPages,
  goLoadMore,
  loadingMore,
}) => {
  const isNoMoreData = pageList.length === totalPages

  const loadMore = (
    <LoadMoreWrap>
      {loadingMore && <DataLoading />}
      {!loadingMore && isNoMoreData && <span>没有更多了</span>}
      {!loadingMore && !isNoMoreData && <Button onClick={goLoadMore}>加载更多页面</Button>}
    </LoadMoreWrap>
  )

  const IconText = ({ title, text }) => ( // eslint-disable-line react/prop-types
    <span>
      <span style={{ marginRight: 8 }}>{title}:</span>
      {text}
    </span>
  )

  const generateEditorUrl = ({ id, url }) => appendQueryStringToUrl('/editor', {
    page_type: 'website',
    website_id: websiteId,
    page_id: id,
    url,
  })

  const ItemTitle = ({ id, url, title }) => ( // eslint-disable-line react/prop-types
    <div>
      <Link to={generateEditorUrl({ id, url })}>{title}</Link>
    </div>
  )

  return (
    <List
      loadMore={loadMore}
      dataSource={pageList}
      renderItem={item => (
        <List.Item
          key={item.id}
          actions={[
            <IconText key="1" title="页面字数" text={item.page_word_count} />,
            <IconText key="5" title="译者" text={item.translator || '无'} />,
            <IconText
              key="6" title="完成度"
              text={handleProgress(item.translateProgress)}
            />,
          ]}
        >
          <List.Item.Meta
            title={
              <ItemTitle
                id={item.id}
                url={item.url}
                title={item.title}
              />
            }
            description={item.url}
          />
        </List.Item>
      )}
    />
  )
}

const WebsiteList = ({
  websiteList,
  totalWebsites,
  isLoadingWebsites,
  isLoadingMorePages,
  goLoadMorePages,
  goLoadMoreWebsites,
  goDeleteWebsite,
  activeId,
  setActiveId,
  requestingItems
}) => {
  const isNoMoreData = websiteList.length === totalWebsites
  const isDeleting = requestingItems.includes('deleteWebsite')

  return (
    <div>
      <Collapse bordered={false}>
        {
          websiteList.map((website) => (
            <StyledPanel
              header={
                HeaderCard(website, goDeleteWebsite, activeId,
                  setActiveId, isDeleting)
              }
              key={website.id}
            >
              <PageList
                websiteId={website.id}
                pageList={website.pages}
                loadingMore={isLoadingMorePages}
                totalPages={website.total_pages}
                goLoadMore={() => goLoadMorePages(website.id)}
              />
            </StyledPanel>
          ))
        }
      </Collapse>
      <LoadMoreWebsites
        isNoMoreData={isNoMoreData}
        loadingMore={isLoadingWebsites}
        goLoadMore={goLoadMoreWebsites}
      />
    </div>
  )
}

WebsiteList.propTypes = {
  requestingItems: PropTypes.array.isRequired,
  websiteList: PropTypes.array.isRequired,
  totalWebsites: PropTypes.number.isRequired,
  isLoadingWebsites: PropTypes.bool.isRequired,
  isLoadingMorePages: PropTypes.bool.isRequired,
  goLoadMorePages: PropTypes.func.isRequired,
  goLoadMoreWebsites: PropTypes.func.isRequired,
  goDeleteWebsite: PropTypes.func.isRequired,
  activeId: PropTypes.string.isRequired,
  setActiveId: PropTypes.func.isRequired
}

export default WebsiteList
