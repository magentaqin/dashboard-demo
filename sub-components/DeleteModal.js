/**
 * Modal for confirming to delete single page/website.
 */

 'use strict'

import { Modal } from 'antd';
import { PropTypes } from 'prop-types'

const { confirm } = Modal

const DeleteModal = (deleteHandler, id, title, initHandler) => {
  confirm({
    title: `确定删除 “${title}” 吗？`,
    content: '删除后，将不能恢复。',
    onOk() {
      deleteHandler(id)
    },
    onCancel() {
      initHandler()
    },
    maskClosable: true
  })
}

DeleteModal.propTypes = {
  deleteHandler: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
}

export default DeleteModal