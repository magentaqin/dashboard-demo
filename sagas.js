/**
 * Sagas, to fetch page and website data.
 */

'use strict'

import { put, takeLatest } from 'redux-saga/effects'
import {
  displayErrorMsg,
  displaySuccessMsg,
  displayInfoMsg,
} from 'utils/messages'
import {
  getPageListApi,
  translatePageApi,

  getWebsiteListApi,
  translateWebsiteApi,

  deletePageApi,
  deleteWebsiteApi
} from 'services/translateService'

import mutations from './mutations'
import {
  sendRequestAction,
  getResponseAction,
  storePagesAction,
  storeWebsitesAction,
  storeWebsiteMorePagesAction,
} from './actions'

function* handleGetPages({ requestParams }) {
  const requestItem = 'pages'
  yield put(sendRequestAction(requestItem))
  const res = yield getPageListApi(requestParams)
  yield put(getResponseAction(requestItem))

  const { data, error } = res
  if (error) {
    displayErrorMsg(error)
    return
  }

  const { page_list, total } = data
  if (!page_list.length) {
    displayInfoMsg('没有数据了')
    return
  }

  yield put(storePagesAction({ page_list, total }))
}

function* watchGetPages() {
  yield takeLatest(mutations['GET_PAGE_LIST_SAGA'], handleGetPages)
}

function* handleGetWebsites({ requestParams }) {
  const requestItem = 'websites'
  yield put(sendRequestAction(requestItem))
  const res = yield getWebsiteListApi(requestParams)
  yield put(getResponseAction(requestItem))

  const { data, error } = res
  if (error) {
    displayErrorMsg(error)
    return
  }

  const { website_list, total } = data
  if (!Object.keys(website_list).length) {
    displayInfoMsg('没有数据了')
    return
  }

  yield put(storeWebsitesAction({ website_list, total }))
}

function* watchGetWebsites() {
  yield takeLatest(mutations['GET_WEBSIE_LIST_SAGA'], handleGetWebsites)
}

function* handleGetWebsiteMorePages({ requestParams }) {
  if (!requestParams.website_id) {
    displayErrorMsg('Website id is required')
    return
  }

  const requestItem = 'morePages'
  yield put(sendRequestAction(requestItem))
  const res = yield getPageListApi(requestParams)
  yield put(getResponseAction(requestItem))

  const { data, error } = res
  if (error) {
    displayErrorMsg(error)
    return
  }

  const { website_id } = requestParams
  const { page_list } = data
  if (!page_list.length) {
    displayInfoMsg('没有数据了')
    return
  }

  yield put(storeWebsiteMorePagesAction({ website_id, page_list }))
}

function* watchGetWebsiteMorePages() {
  yield takeLatest(mutations['GET_WEBSITE_MORE_PAGES_SAGA'],
    handleGetWebsiteMorePages)
}

function* handleTranslationRequest({ metaData, category }) {
  const requestApiMap = {
    'page': {
      'api': translatePageApi,
      'refresh': mutations['GET_PAGE_LIST_SAGA'],
    },
    'website': {
      'api': translateWebsiteApi,
      'refresh': mutations['GET_WEBSIE_LIST_SAGA'],
    },
  }
  const requestApi = requestApiMap[category]['api']

  // clear submit result in store
  yield put({
    type: mutations['STORE_SUBMIT_RESULT'],
    result: '',
    category,
  })

  // 提交翻译
  const requestItem = 'submitTranslation'
  yield put(sendRequestAction(requestItem))
  const res = yield requestApi(metaData)
  yield put(getResponseAction(requestItem))

  const { data, error } = res
  if (error) {
    displayErrorMsg(error)
    return
  }

  displaySuccessMsg('提交翻译成功!')
  yield put({
    type: mutations['STORE_SUBMIT_RESULT'],
    result: 'success',
    category,
    data,
  })

  // refresh data store, Updated: Just add new one localy.
  // const refreshCommand = requestApiMap[category]['refresh']
  // yield put({ type: refreshCommand, requestParams: {} })
}

function* watchTranslationRequest() {
  yield takeLatest(mutations['SUBMIT_NEW_TRANSLATION_SAGA'],
    handleTranslationRequest)
}


function* handleDeletePage({ pageId }) {
  if (!pageId) {
    displayErrorMsg('Page id required')
    return
  }

  const requestItem = 'deletePage'
  yield put(sendRequestAction(requestItem))
  const res = yield deletePageApi(pageId)
  yield put(getResponseAction(requestItem))

  const { data, error } = res
  if (error) {
    displayErrorMsg(error)
    return
  }

  if (data) {
    displaySuccessMsg('删除页面成功')
    yield put({ type: mutations['DELETE_PAGE'], pageId })
  }
}

function* handleDeleteWebsite({ websiteId }) {
  if (!websiteId) {
    displayErrorMsg('Website id required')
    return
  }

  const requestItem = 'deleteWebsite'
  yield put(sendRequestAction(requestItem))
  const res = yield deleteWebsiteApi(websiteId)
  yield put(getResponseAction(requestItem))

  const { data, error } = res
  if (error) {
    displayErrorMsg(error)
    return
  }

  if (data) {
    displaySuccessMsg('删除网站成功')
    yield put({ type: mutations['DELETE_WEBSITE'], websiteId })
  }
}

function* watchHandleDeletePage() {
  yield takeLatest(mutations['DELETE_PAGE_SAGA'], handleDeletePage)
}

function* watchHandleDeleteWebsite() {
  yield takeLatest(mutations['DELETE_WEBSITE_SAGA'], handleDeleteWebsite)
}

export default [
  watchGetPages(),
  watchGetWebsites(),
  watchGetWebsiteMorePages(),
  watchTranslationRequest(),
  watchHandleDeletePage(),
  watchHandleDeleteWebsite()
]