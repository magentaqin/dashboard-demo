/**
 * Dashboard component
 * @author qinmu
 */

'use strict'

import React from 'react'
import { PropTypes } from 'prop-types'
import styled from 'styled-components'
import { Tabs, Layout, Icon } from 'antd'
import isEqual from 'lodash.isequal'

import { getQueryObjFromUrl, appendQueryStringToUrl } from 'utils/qs'
import {
  displayErrorMsg,
} from 'utils/messages'

// Import sub components
import Footer from './sub-components/Footer'
import Header from './sub-components/Header'
import CreateTranslationModal from './sub-components/CreateModal'
import PageList from './sub-components/PageList'
import WebsiteList from './sub-components/WebsiteList'

const TabPane = Tabs.TabPane
const { Content } = Layout

const pageTabsMap = ['page', 'website']

const Wrapper = styled.div`
  width: 80%;
  max-width: 1600px;
  margin: 0 auto;
`
const StyledLayout = styled(Layout) `
  display: flex;
  align-content: space-around;
  flex-direction: column;
  min-height: 100vh;
`
const StyledContent = styled(Content) `
  flex: 1;
  padding: 25px 50px;
`
const InnerContent = Wrapper.extend`
  min-height: 600px;
  padding: 50px;
  background-color: #fff;
`
const StyledTabPane = styled(TabPane) `
  min-height: 400px;
`
class DashboardComponent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isShowModal: false,
      deletingPageId: '',
      activePageId: '',
      activeWebsiteId: ''
    }
  }

  componentDidMount() {
    // load list data
    this.handleLoadData()
  }

  componentWillReceiveProps(nextProps) {
    const {
      router,
    } = this.props

    if (!isEqual(nextProps.router, router)) {
      this.handleLoadData(nextProps)
    }
  }

  setActivePageId = (id) => {
    this.setState({ activePageId: id })
  }

  setActiveWebsiteId = (id) => {
    this.setState({ activeWebsiteId: id })
  }

  getDataStoreSize = (type) => {
    const {
      pageList,
      websiteList,
    } = this.props

    const websiteListSize = Object.keys(websiteList).length
    const pageListSize = pageList.length

    const typeListMap = {
      'page': pageListSize,
      'website': websiteListSize,
    }

    if (type && typeListMap[type] !== undefined) {
      return typeListMap[type]
    }

    return 0
  }

  handleLoadData = (newProps) => {
    const props = newProps || this.props
    const {
      goLoadPages,
      goLoadWebsiteMorePages,
      goLoadWebsites,
      websiteList,
      history,
      totalPages,
      totalWebsites,
    } = this.props

    const pageTypeTotalSizeMap = {
      'page': totalPages,
      'website': totalWebsites,
    }


    const { pathname, search } = props.router.location
    const {
      page_type = 'page',
      base = 0,
      count,
      website_id = '',
    } = getQueryObjFromUrl(search)

    const defaultCount = 10

    // Total count requesting items.
    let totalRecords = parseInt(base)

    // Container to record requesting parameters
    const requestParams = { base }

    if (!base && !count) {
      const newUrl = appendQueryStringToUrl(pathname, {
        page_type,
        base: 0,
        count: defaultCount,
      })
      history.replace(newUrl)
    }

    if (count && parseInt(count)) {
      totalRecords += parseInt(count)
      requestParams['count'] = count
    }

    // Datas already fetched from server
    const dataSize = this.getDataStoreSize(page_type)

    // Total records count in db.
    const totalDataSize = pageTypeTotalSizeMap[page_type]

    // If all records fetched from server, do not send requests anymore.
    if (!website_id) {
      if (dataSize && totalDataSize && dataSize === totalDataSize) {
        return
      }
    }

    if (!dataSize && !totalRecords) {
      totalRecords += defaultCount
      requestParams['count'] = defaultCount
    }

    if (!dataSize && parseInt(base)) {
      const newUrl = appendQueryStringToUrl(pathname, {
        page_type,
        base: 0,
        count: totalRecords,
      })
      history.replace(newUrl)
      return
    }

    if (page_type === 'page' && dataSize < totalRecords) {
      goLoadPages({ requestParams })
    }

    if (page_type === 'website') {
      if (website_id && websiteList[website_id]) {
        const targetWebsite = websiteList[website_id]
        const websitePagesSzie = targetWebsite.pages.length
        // If all pages fetched, do not fetch anymore.
        if (targetWebsite.total_pages === websitePagesSzie) {
          return
        }
        if (websitePagesSzie < totalRecords) {
          requestParams['website_id'] = website_id
          goLoadWebsiteMorePages({ requestParams })
        }
      } else if (dataSize < totalRecords) {
        goLoadWebsites({ requestParams })
      }
    }
  }

  changeTab = (activeKey) => {
    const page_type = pageTabsMap[activeKey]
    const { history, location } = this.props
    const base = this.getDataStoreSize(page_type)

    const qsObj = { base, page_type }

    const nextUrl = appendQueryStringToUrl(location.pathname, qsObj)
    history.push(nextUrl)
  }

  goLoadMore = (websiteId) => {
    // go to load more data
    const {
      router,
      history,
      websiteList,
    } = this.props

    const { search, pathname } = router.location
    const { page_type = 'page' } = getQueryObjFromUrl(search)

    let base = this.getDataStoreSize(page_type)

    let website_id = ''

    if (websiteId && typeof (websiteId) === 'string') {
      const targetWebsite = websiteList[websiteId]
      if (!targetWebsite) {
        displayErrorMsg('没有找到该网站')
        return
      }
      base = targetWebsite.pages.length
      website_id = websiteId
    }

    let qsObj = { page_type, base, count: 10 }
    if (website_id) {
      qsObj = { ...qsObj, ...{ website_id } }
    }

    const nextUrl = appendQueryStringToUrl(pathname, qsObj)
    history.push(nextUrl)
  }

  // handlers for translation modal.
  createNewTranslation = () => {
    this.setState({ isShowModal: true })
  }

  closeTranslationModal = () => {
    this.setState({ isShowModal: false })
  }

  // handlers for notification
  showNotification = () => {
    console.log('no more notifications')
  }

  markAllRead = () => {
    console.log('mark all notices readed')
  }

  logout = () => {
    // User logout
    const { goLogout } = this.props
    goLogout()
  }

  setDeletingPageId = (pageId) => this.setState({ deletingPageId: pageId })

  renderPageList = () => {
    const {
      requestingItems,
      pageList,
      totalPages,
      goDeletePage,
    } = this.props

    const { deletingPageId, activePageId } = this.state

    return (
      <PageList
        requestingItems={requestingItems}
        pageList={pageList}
        totalPages={totalPages}
        goLoadMore={this.goLoadMore}
        goDeletePage={goDeletePage}
        deletingPageId={deletingPageId}
        setDeletingPageId={this.setDeletingPageId}
        activeId={activePageId}
        setActiveId={this.setActivePageId}
      />
    )
  }

  renderWebsiteList = () => {
    const {
      totalWebsites,
      requestingItems,
      sortedWebsiteList,
      goDeleteWebsite
    } = this.props

    const { activeWebsiteId } = this.state

    return (
      <WebsiteList
        websiteList={sortedWebsiteList}
        totalWebsites={totalWebsites}
        isLoadingWebsites={requestingItems.includes('websites')}
        isLoadingMorePages={requestingItems.includes('morePages')}
        goLoadMorePages={(websiteId) => this.goLoadMore(websiteId)}
        goLoadMoreWebsites={this.goLoadMore}
        goDeleteWebsite={goDeleteWebsite}
        activeId={activeWebsiteId}
        setActiveId={this.setActiveWebsiteId}
        requestingItems={requestingItems}
      />
    )
  }

  render() {
    const {
      router,
      requestingItems,
      submitNewTranslation,
      submitResult,
      accountInfo,
    } = this.props

    const { username } = accountInfo

    const {
      isShowModal,
    } = this.state

    const { page_type = 'page' } = getQueryObjFromUrl(router.location.search)
    let activeKey = pageTabsMap.indexOf(page_type)
    activeKey = activeKey !== -1 ? activeKey.toString() : '1'

    const pageTab = (<span><Icon type="file-text" />单个页面列表</span>)
    const websiteTab = (<span><Icon type="desktop" />网站列表</span>)

    return (
      <StyledLayout className="layout dashboard-component">
        <CreateTranslationModal
          isShowModal={isShowModal}
          closeModal={this.closeTranslationModal}
          requestingItems={requestingItems}
          submitNewTranslation={submitNewTranslation}
          submitResult={submitResult}
        />
        <Header
          username={username}
          createNewTranslation={this.createNewTranslation}
          showNotification={this.showNotification}
          notificationsCount={0}
          goLogout={this.logout}
        />
        <StyledContent>
          <InnerContent>
            <Tabs
              defaultActiveKey="0"
              activeKey={activeKey}
              animated={false}
              size='large'
              onChange={this.changeTab}
            >
              <StyledTabPane tab={pageTab} key="0">
                {this.renderPageList()}
              </StyledTabPane>
              <StyledTabPane tab={websiteTab} key="1">
                {this.renderWebsiteList()}
              </StyledTabPane>
            </Tabs>
          </InnerContent>
        </StyledContent>
        <Footer />
      </StyledLayout>
    )
  }
}

DashboardComponent.propTypes = {
  router: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  accountInfo: PropTypes.object.isRequired,
  requestingItems: PropTypes.array.isRequired,
  submitResult: PropTypes.string.isRequired,
  pageList: PropTypes.array.isRequired,
  websiteList: PropTypes.object.isRequired,
  sortedWebsiteList: PropTypes.array.isRequired,
  totalPages: PropTypes.number.isRequired,
  totalWebsites: PropTypes.number.isRequired,
  submitNewTranslation: PropTypes.func.isRequired,
  goLoadPages: PropTypes.func.isRequired,
  goLoadWebsites: PropTypes.func.isRequired,
  goLoadWebsiteMorePages: PropTypes.func.isRequired,
  goLogout: PropTypes.func.isRequired,
  goDeletePage: PropTypes.func.isRequired,
  goDeleteWebsite: PropTypes.func.isRequired
}

export default DashboardComponent