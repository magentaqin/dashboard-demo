/**
 * Dashboard reducer store
 * @author qinmu
 */

'use strict'

import mutations from './mutations'

const initialState = {
  'requestingItems': [],    // 'pages', 'websites', 'morePages', 'submitTranslation'
  'submitResult': '',
  'pageList': [],     // a list of page metadata
  'totalPages': -1,
  'websiteList': {},  // website_id: website_metadata
  'totalWebsites': -1,  // NOTE: Make default to be -1, just to check if request has sent.
}

function dashboardReducer(state = initialState, action) {
  switch (action.type) {
    case mutations['REQUESTING_ITEM']: {
      const items = [...state.requestingItems]
      items.push(action.item)
      return { ...state, requestingItems: items }
    }

    case mutations['FINISH_ITEM']: {
      let items = [...state.requestingItems]
      items = items.filter((item) => item !== action.item)
      return { ...state, requestingItems: items }
    }

    case mutations['STORE_SUBMIT_RESULT']: {
      const { result, category, data } = action
      const resultState = { ...state, submitResult: result }
      if (!data) {  // If no data attached, just return
        return resultState
      }

      // If already exists, or no data fetched,
      if (category === 'page') {
        const { pageList, totalPages } = state
        const page_found = pageList.filter(page => page.id === data.id)
        if (totalPages !== -1 && !page_found.length) {
          pageList.unshift(data)      // Add data to the beggining of data list
          resultState['pageList'] = pageList
          resultState['totalPages'] = totalPages + 1
        }
      } else if (category === 'website') {
        const { websiteList, totalWebsites } = state
        if (totalWebsites !== -1 && websiteList[data.id] === undefined) {
          const dataMap = { [data.id]: data }
          resultState['websiteList'] = { ...websiteList, ...dataMap }
          resultState['totalWebsites'] = totalWebsites + 1
        }
      }

      return resultState
    }

    case mutations['GET_PAGE_LIST']: {
      const { page_list, total } = action
      let { pageList } = state
      pageList = [...pageList, ...page_list]
      return {
        ...state,
        pageList,
        totalPages: total,
      }
    }

    case mutations['GET_WEBSIE_LIST']: {
      const { website_list, total } = action
      const newWebsiteList = { ...state.websiteList, ...website_list }

      return {
        ...state,
        websiteList: newWebsiteList,
        totalWebsites: total,
      }
    }

    case mutations['GET_WEBSITE_MORE_PAGES']: {
      const { website_id, page_list } = action
      if (!website_id || !page_list) {
        return state
      }

      const { websiteList } = state
      const targetWebsite = websiteList[website_id]
      if (!targetWebsite) {
        return state
      }

      targetWebsite['pages'] = [...targetWebsite['pages'], ...page_list]
      websiteList[website_id] = targetWebsite

      return { ...state, websiteList }
    }

    case mutations['DELETE_PAGE']: {
      const { pageId } = action
      if (!pageId) {
        return state
      }

      const resultPages = state.pageList.filter(page => page.id !== pageId)
      return { ...state, pageList: resultPages, totalPages: state.totalPages - 1 }
    }

    case mutations['DELETE_WEBSITE']: {
      const { websiteId } = action
      if (!websiteId) {
        return state
      }
      delete state.websiteList[websiteId]
      const resultWebsites = state.websiteList
      return { ...state, websiteList: resultWebsites, totalWebsites: state.totalWebsites - 1}
    }

    default:
      return state
  }
}

export default dashboardReducer
