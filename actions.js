/**
 * Actions
 */

'use strict'

import mutations from './mutations'

export const sendRequestAction = (item) => ({
  type: mutations['REQUESTING_ITEM'],
  item,
})

export const getResponseAction = (item) => ({
  type: mutations['FINISH_ITEM'],
  item,
})

export const storePagesAction = ({ page_list, total }) => ({
  type: mutations['GET_PAGE_LIST'],
  page_list,
  total,
})

export const storeWebsitesAction = ({ website_list, total }) => ({
  type: mutations['GET_WEBSIE_LIST'],
  website_list,
  total,
})

export const storeWebsiteMorePagesAction = ({ website_id, page_list }) => ({
  type: mutations['GET_WEBSITE_MORE_PAGES'],
  website_id,
  page_list,
})

// category:  page || website
export const submitNewTranslationAction = ({ metaData, category }) => ({
  type: mutations['SUBMIT_NEW_TRANSLATION_SAGA'],
  metaData,
  category,
})